#!/bin/bash
#SBATCH --nodes=1
#SBATCH --gres=gpu:1
#SBATCH --ntasks=1
#SBATCH --mem=4G # memory pool for all cores
#SBATCH --time=2-2:00 # time (D-HH:MM)
#SBATCH --account=ccorbi
#SBATCH -o slurm.%N.%j.out # STDOUT
#SBATCH -e slurm.%N.%j.err # STDERR
#SBATCH --node-list=beagle41

nvidia-smi

source activate amber
export AMBER_PREFIX=/home/kimlab2/ccorbi/amber16/
export AMBERHOME=/home/kimlab2/ccorbi/amber16/
export PATH="${AMBER_PREFIX}/bin:${PATH}"
# Add location of Amber Python modules to default Python search path
if [ -z "$PYTHONPATH" ]; then
        export PYTHONPATH="${AMBER_PREFIX}/lib/python2.7/site-packages"
    else
        export PYTHONPATH="${AMBER_PREFIX}/lib/python2.7/site-packages:${PYTHONPATH}"
fi

if [ -z "${LD_LIBRARY_PATH}" ]; then
        export LD_LIBRARY_PATH="${AMBER_PREFIX}/lib"
    else
        export LD_LIBRARY_PATH="${AMBER_PREFIX}/lib:${LD_LIBRARY_PATH}"
fi

#export CUDA_HOME="/usr/local/cuda"
export LD_LIBRARY_PATH="/home/kimlab2/ccorbi/anaconda/envs/amber/lib:${LD_LIBRARY_PATH}"
export LD_LIBRARY_PATH="/home/kimlab2/ccorbi/anaconda/lib:${LD_LIBRARY_PATH}"

#export LD_LIBRARY_PATH="/usr/local/cuda/lib:${LD_LIBRARY_PATH}"
#device=`setGPU`
#export CUDA_VISIBLE_DEVICES="${device}"
#echo $CUDA_VISIBLE_DEVICES
export TEMPDIR='/tmp/'


pmemd.cuda -O -i 1-min.inp -o min.1.out -p finger.parm7 -c finger.rst7 -r min.1.rst7 -ref finger.rst7
pmemd.cuda -O -i 2-min.inp -o min.2.out -p finger.parm7 -c min.1.rst7 -r min.2.rst7

pmemd.cuda -O -i heat.inp -o heat.out -p finger.parm7 -c min.2.rst7 -r heat.rst7 -ref min.2.rst7
pmemd.cuda -O -i dens.inp -o dens.out -p finger.parm7 -c  heat.rst7 -r dens.rst7 -ref heat.rst7


pmemd.cuda -O -i 3-equil.inp -o equil.3.out -p finger.parm7 -c dens.rst7 -r equil.3.rst7 -ref dens.rst7
pmemd.cuda -O -i 4-equil.inp -o equil.4.out -p finger.parm7 -c  equil.3.rst7 -r equil.4.rst7 -ref equil.3.rst7

pmemd.cuda  -O -i md.1.inp -o md.1.out -p finger.parm7 -c  equil.4.rst7 -x md.1.mdcrd  -r md.1.rst7 -ref equil.4.rst7

for N in {2..3}
do
pmemd.cuda -O -i md.3.inp -o md.$N.out -p finger.parm7 -c  md.$(($N-1)).rst7 -x md.$N.mdcrd  -r md.$N.rst7
done
